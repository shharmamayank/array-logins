const arrayLogins = require("./2-arrays-logins.cjs")
function sumAllTheFourthIpAddress(arrayLogins) {
    let arr = []
    if (!Array.isArray(arrayLogins)) {
        return arr
    } else {
        const sumOfFourthIpAddress = arrayLogins.map(data => {
            return data.ip_address.split(".").slice(3)
        }).reduce((previous, current) => {
            return (+previous) + (+current)
        })
        return sumOfFourthIpAddress
    }
}
console.log(sumAllTheFourthIpAddress(arrayLogins))