const arrayLogins = require("./2-arrays-logins.cjs")
function sumAllTheSecondIpAddress(arrayLogins) {
    let arr = []
    if (!Array.isArray(arrayLogins)) {
        return arr
    } else {
        const sumOfSecondIpAddress = arrayLogins.map(data => {
            return data.ip_address.split(".").slice(1,2)
        }).reduce((previous, current) => {
            return (+previous) + (+current)
        })
        return sumOfSecondIpAddress
    }
}
console.log(sumAllTheSecondIpAddress(arrayLogins))