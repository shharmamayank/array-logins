const arrayLogins = require("./2-arrays-logins.cjs")
function getTheFullName(arrayLogins) {
    let arr = []
    if (!Array.isArray(arrayLogins)) {
        return arr
    } else {
        const getTheFullName = arrayLogins.map(data => {
            data["FullName"] = data.first_name + " " + data.last_name
            return data
        })
        return getTheFullName
    }
}
console.log(getTheFullName(arrayLogins))