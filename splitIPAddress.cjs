const arrayLogins = require("./2-arrays-logins.cjs")
function SplitIpAddressIntoComponents(params) {
    let arr = []
    if (!Array.isArray(arrayLogins)) {
        return arr
    } else {
        const componentInsplitedIpAddress = arrayLogins.map(data => {
            return data.ip_address.split(".")
        })
        return componentInsplitedIpAddress
    }
}

console.log(SplitIpAddressIntoComponents(arrayLogins))