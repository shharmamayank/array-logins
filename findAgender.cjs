const arrayLogins = require("./2-arrays-logins.cjs")
let arr = []
function findAgender(arrayLogins) {
    if (!Array.isArray(arrayLogins)) {
        return arr
    } else {
        const findAgender = arrayLogins.filter(data => {
            return data.gender.includes("Agender")
        })
        return findAgender
    }
}
console.log(findAgender(arrayLogins))